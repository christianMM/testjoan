/**
 * @file script.js
 */

var wt = {};


/**
 * Global variables
 */

wt.vars = {

  mainTheme:      null,
  randomWord:     null,
  setOfLetters:   [],
  level:          1,
  rugLetters:     200
}

/**
 * Methods related to the app
 */

wt.audio = {

  

  preloadMainTheme: function() {

    wt.vars.mainTheme = new Audio('http://wordtap.donbuche.com/sounds/main_screen.mp3');
  },

  loadMainTheme: function() {
    wt.vars.mainTheme.play();
  }
};

/**
 * Methods related to the app
 */

wt.app = {

  init: function() {

    var $content = $('.loading-screen .content');
    var $loading = $('.loading-screen');
    var $main = $('.main-screen');

    $content.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
      $loading.delay(2000).animate({'opacity': '0'}, function() {
        $loading.addClass('hidden');
        $main.removeClass('hidden');
        wt.audio.loadMainTheme();
      });
    });
  },

  startGame: function() {

    var $main = $('.main-screen');
    var $game = $('.game-screen');
    
    $main.delay(2000).animate({'opacity': '0'}, function() {
      $main.addClass('hidden');
      $game.removeClass('hidden');
    });
  }
};

/**
 * Methods related to the game
 */

wt.game = {

  setLevel: function() {

    $('.level span').text(wt.vars.level);
  },

  updateLevel: function() {

    wt.vars.level++;
  },

  resetWord: function() {

    $('#target-word').empty();
  },

  newWord: function() {

    $.getJSON('../json/10-letter-words.json', function(data) {

      var i = 0;
      var length = data.length;
      var value = Math.floor(Math.random() * length) + 1;
      
      $.each(data, function(k, v) {
        
        if (i == value) {
          wt.vars.randomWord = v;
        }
        i++;
      });

    }).done(function() {

      var chars = wt.vars.randomWord.word;
      chars = chars.split('');
      
      for (j = 0; j < chars.length; j++) {
        $('<span class="char char-' + chars[j] + ' unselectable"><span>' + chars[j] + '</span></span>').appendTo($('#target-word'));
      }

      wt.game.newSetOfLetters(wt.vars.rugLetters);
    });
  },

  resetSetOfLetters: function() {

    $('#letters-area').empty();
  },

  newSetOfLetters: function(quantity) {

    $.getJSON('../json/letters.json', function(data) {

      var i = 0;
      var j = 0;
      var v = wt.vars.randomWord.word;
      var w = v.split('');
      var x = 0;
            
      for (i = 0; i <= quantity; i++) {
        j = Math.floor(Math.random() * 25) + 0;
        wt.vars.setOfLetters.push(data[j]);
        i++;
      }

      for (x = 0; x < w.length; x++) {
        wt.vars.setOfLetters.push(w[x]);
        i++;
      }

    }).done(function() {

      var x = 0;
      var y = 0;
      var z = 0;

      for (i = 0; i < wt.vars.setOfLetters.length; i++) {
        x = Math.floor(Math.random() * 90) + 1;
        y = Math.floor(Math.random() * 90) + 1;
        z = Math.floor(Math.random() * quantity) + 1;
        
        $('<span class="letter-container" style="top: ' + x + '%; left: ' + y + '%; z-index: ' + z + '"><span class="letter unselectable"><span>' + wt.vars.setOfLetters[i] + '</span></span></span>').appendTo($('#letters-area'));
        i++;
      }
      wt.event.letterClick();
      wt.event.draggableLetters();
    });
  }
};

/**
 * Methods related to events
 */

wt.event = {

  startGame: function() {

    $('.start-game button').click(function() {
      wt.app.startGame();
      wt.game.newWord();
    });
  },

  resetWord: function() {

    $('.restart').click(function() {
      swal({
        title: 'Restart game',
        text: "Are you sure?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#dd3333',
        confirmButtonText: 'Restart',
        closeOnConfirm: true
      }).then(function(isConfirm) {
        if (isConfirm) {
          wt.game.resetWord();
          wt.game.newWord();
          wt.game.resetSetOfLetters();
          wt.vars.setOfLetters = []; // Thanks, Mr. Llansola!
        }
      })
    });
  },

  letterClick: function() {

    $('.letter').click(function() {
      
      var value = $(this).text();
      var word  = wt.vars.randomWord.word;

      if (word.indexOf(value) != -1) {
        $('.char.char-' + value).addClass('set');
        wt.event.updateScore();
      }

      wt.event.newLevel();
    });
  },

  newLevel: function() {

    var a = $('#target-word .char').length;
    var b = $('#target-word .char.set').length;

    if (a == b) {
      $('#target-word .char.set').addClass('complete');
      swal({
        title: 'Congratulations!',
        text:  'Ready to try next level?',
        type:  'success',
        allowOutsideClick: false,
        allowEscapeKey: false
      }).then(function(isConfirm) {
        if (isConfirm) {
          wt.game.updateLevel();
          wt.game.setLevel();
          wt.game.resetWord();
          wt.game.newWord();
          wt.game.resetSetOfLetters();
          wt.vars.setOfLetters = [];
        }
      });
    }
  },

  playAudioManually: function() {

    $('.play-audio').click(function() {
      wt.vars.mainTheme.play();
    });
  },

  updateScore: function() {

    var score = parseInt($('.score span').text());
    var newScore = score + 10;
    $('.score span').text(newScore);
  },

  showInfo: function() {

    $('.options').click(function() {
      swal({
        title: 'How to play',
        html:
          'Complete the suggested word shown in the upper toolbar ' +
          'by double clicking on the letters of the rug.<br/><br/>' +
          'Feel free to dragging the letters to reveal hidden letters, but ' +
          'do it quickly, before the timer run out!',
        allowOutsideClick: false,
        allowEscapeKey: false
      })
    });
  },

  draggableLetters: function() {

    interact('.letter-container')
    .draggable({
      inertia: true,
      restrict: {
        restriction: '#letters-area',
        endOnly: true,
        elementRect: {
          top: 0, 
          left: 0, 
          bottom: 1, 
          right: 1
        }
      },
      autoScroll: true,
      onmove: dragMoveListener,
      onend: function(event) {
        var textEl = event.target.querySelector('p');
        textEl && (textEl.textContent = 'moved a distance of ' + (Math.sqrt(event.dx * event.dx + event.dy * event.dy)|0) + 'px');
      }
    });

    function dragMoveListener (event) {
      var target = event.target,
          x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
          y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    }
    window.dragMoveListener = dragMoveListener;
  }
};







/**
 * Method's calls on DOM Ready event
 */

$(function() {


  $('.letter-container').draggable();

  // Preload audio files
  
  wt.audio.preloadMainTheme();
  
  // App
  
  wt.app.init();

  // Events
  
  wt.event.startGame();
  wt.event.resetWord();
  wt.event.playAudioManually();
  wt.event.showInfo();



});